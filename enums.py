# -*- coding: utf-8 -*-


# Данная функция позволяет генерировать перечисления
def enum(*sequential, **named):
    enums = dict(zip(sequential, range(len(sequential))), **named)
    return type('Enum', (), enums)


# Какие стороны играют. Это у нас белые и черные
Sides = enum('WHITE', 'BLACK')


# Какие фигуры могут быть
Pieces = enum('WHITE_KING', 'WHITE_QUEEN', 'WHITE_ROOK', 'WHITE_KNIGHT', 'WHITE_BISHOP', 'WHITE_PAWN',
              'BLACK_KING', 'BLACK_QUEEN', 'BLACK_ROOK', 'BLACK_KNIGHT', 'BLACK_BISHOP', 'BLACK_PAWN')


# Статус игры
GameStatus = enum('THE_GAME_IS_ONGOING', 'WHITE_HAS_BEEN_CHECKMATED', 'BLACK_HAS_BEEN_CHECKMATED', 'STALEMATE',
	              'WHITE_RESIGNS', 'BLACK_RESIGNS',
	              'WHITE_WON_ON_TIME', 'BLACK_WON_ON_TIME', 'WHITE_WON_BY_FORFEIT', 'BLACK_WON_BY_FORFEIT',
	              'THREEFOLD_REPETITION', 'FIFTY_MOVE_RULE', 'FIVEFOLD_REPETITION', 'SEVENTY_FIVE_MOVE_RULE',
	              'INSUFFICIENT_MATERIAL', 'DRAW_ON_TIME',
                  'WHITE_OFFER_A_DRAW', 'BLACK_OFFER_A_DRAW', 'WHITE_ACCEPT_THE_DRAW', 'BLACK_ACCEPT_THE_DRAW')


# Результат игры
GameResult = enum('THE_GAME_IS_ONGOING', 'WHITE_WON', 'DRAW', 'BLACK_WON')


# Цвет клеток
CellColor = enum('WHITE', 'BLACK')


# Все фигуры
ALL_PIECES = [Pieces.WHITE_KING, Pieces.WHITE_QUEEN, Pieces.WHITE_ROOK, Pieces.WHITE_KNIGHT, Pieces.WHITE_BISHOP, Pieces.WHITE_PAWN,
              Pieces.BLACK_KING, Pieces.BLACK_QUEEN, Pieces.BLACK_ROOK, Pieces.BLACK_KNIGHT, Pieces.BLACK_BISHOP, Pieces.BLACK_PAWN]


# Все фигуры, кроме пешек
NOT_PAWN_PIECES = [Pieces.WHITE_KING, Pieces.WHITE_QUEEN, Pieces.WHITE_ROOK, Pieces.WHITE_KNIGHT, Pieces.WHITE_BISHOP,
                   Pieces.BLACK_KING, Pieces.BLACK_QUEEN, Pieces.BLACK_ROOK, Pieces.BLACK_KNIGHT, Pieces.BLACK_BISHOP]


# Белые фигуры
WHITE_PIECES = [Pieces.WHITE_KING, Pieces.WHITE_QUEEN, Pieces.WHITE_ROOK, Pieces.WHITE_KNIGHT, Pieces.WHITE_BISHOP, Pieces.WHITE_PAWN]


# Черные фигуры
BLACK_PIECES = [Pieces.BLACK_KING, Pieces.BLACK_QUEEN, Pieces.BLACK_ROOK, Pieces.BLACK_KNIGHT, Pieces.BLACK_BISHOP, Pieces.BLACK_PAWN]


# Потенциально возможные новые фигуры
POSSIBLE_NEW_PIECES = [Pieces.WHITE_QUEEN, Pieces.WHITE_ROOK, Pieces.WHITE_KNIGHT, Pieces.WHITE_BISHOP,
                       Pieces.BLACK_QUEEN, Pieces.BLACK_ROOK, Pieces.BLACK_KNIGHT, Pieces.BLACK_BISHOP]

