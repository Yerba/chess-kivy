# -*- coding: utf-8 -*-

# Импорт модулей
from enums import Sides, Pieces


# Класс позиции
class Position:

    # Инициализация класса позиция
    def __init__(self, notation=None):
    
        # Вначале считаем, что на доске все пусто
        self.piece_placement = []
        for row in range(8):
            self.piece_placement.append([])
            for col in range(8):
                self.piece_placement[row].append(None)
            
        # Очередь хода у белых
        self.whose_turn = Sides.WHITE
        
        # Естественно обе стороны не могут рокироваться
        self.white_can_kings_side_castling = False
        self.white_can_queens_side_castling = False
        self.black_can_kings_side_castling = False
        self.black_can_queens_side_castling = False
        
        # Проходной пешки нет
        self.en_passant_target_square = None
        
        # Счетчик полуходов равен нулю
        self.halfmove_clock = 0
        
        # Номер хода равен единице
        self.fullmove_number = 1
        
        # Если позиция задана в виде нотации Форсайта-Эдвардса,
        # то восстанавливаем её
        if notation is not None:
        
            # Опишем для себя какая фигура как обозначается
            set_all_pieces = {'K': Pieces.WHITE_KING,
                              'Q': Pieces.WHITE_QUEEN,
                              'R': Pieces.WHITE_ROOK,
                              'N': Pieces.WHITE_KNIGHT,
                              'B': Pieces.WHITE_BISHOP,
                              'P': Pieces.WHITE_PAWN,
                              'k': Pieces.BLACK_KING,
                              'q': Pieces.BLACK_QUEEN,
                              'r': Pieces.BLACK_ROOK,
                              'n': Pieces.BLACK_KNIGHT,
                              'b': Pieces.BLACK_BISHOP,
                              'p': Pieces.BLACK_PAWN}
                               
            # А теперь разбираемся, где какая фигура стоит
            row = 7; col = 0; ind = 0
            while row >= 0:
                ch = notation[ind]
                if ch in set_all_pieces:
                    self.piece_placement[row][col] = set_all_pieces[ch]
                    col += 1
                elif ord(ch) in range(ord('1'), ord('9') + 1):
                    col += ord(ch) - ord('1') + 1
                if col == 8:
                    ind += 1; row -= 1; col = 0
                ind += 1
            
            # Убираем пробельные символы
            while notation[ind].isspace():
                ind += 1
                      
            # Определяем чья очередь хода
            if notation[ind] in 'wb':
                if notation[ind] == 'b':
                    self.whose_turn = Sides.BLACK
                ind += 1
            
            # Продолжаем дальше считывать
            while notation[ind].isspace():
                ind += 1
                
            # Определяем можно ли рокироваться или нет
            while notation[ind] in 'KQkq-':
                if notation[ind] == 'K':
                    self.white_can_kings_side_castling = True
                elif notation[ind] == 'Q':
                    self.white_can_queens_side_castling = True
                elif notation[ind] == 'k':
                    self.black_can_kings_side_castling = True
                elif notation[ind] == 'q':
                    self.black_can_queens_side_castling = True
                ind += 1
                
            # Продолжаем дальше считывать
            while notation[ind].isspace():
                ind += 1
                
            # Разбираемся с проходной клеткой            
            if notation[ind] in 'abcdefgh':
                x = ord(notation[ind]) - ord('a'); ind += 1
                if notation[ind] in '12345678':
                    y = ord(notation[ind]) - ord('1'); ind += 1
                    self.en_passant_target_square = (y, x)
            elif notation[ind] == '-':
                ind += 1
                
            # Продолжаем дальше считывать
            while notation[ind].isspace():
                ind += 1

            # Считываем счётчик полуходов
            while notation[ind] in '0123456789':
                self.halfmove_clock = self.halfmove_clock * 10 + ord(notation[ind]) - ord('0')
                ind += 1
                
            # Продолжаем дальше считывать
            while notation[ind].isspace():
                ind += 1
                
            # Считываем номер хода
            self.fullmove_number = 0
            while (ind < len(notation)) and (notation[ind] in '0123456789'):
                self.fullmove_number = self.fullmove_number * 10 + ord(notation[ind]) - ord('0')
                ind += 1
                
    # Возвращает стандартную нотацию данной позиции
    def notation(self):
    
        # Начинаем всё с пустой строки
        result = ""
        
        # Описываем положение фигур со стороны белых
        for row in range(7, -1, -1):
            empty_squares_number = 0
            for col in range(8):
                if self.piece_placement[row][col] is None:
                    empty_squares_number += 1
                elif self.piece_placement[row][col] == Pieces.WHITE_KING:
                    if empty_squares_number > 0: result += str(empty_squares_number)
                    empty_squares_number = 0
                    result += "K"
                elif self.piece_placement[row][col] == Pieces.WHITE_QUEEN:
                    if empty_squares_number > 0: result += str(empty_squares_number)
                    empty_squares_number = 0
                    result += "Q"
                elif self.piece_placement[row][col] == Pieces.WHITE_ROOK:
                    if empty_squares_number > 0: result += str(empty_squares_number)
                    empty_squares_number = 0
                    result += "R"
                elif self.piece_placement[row][col] == Pieces.WHITE_KNIGHT:
                    if empty_squares_number > 0: result += str(empty_squares_number)
                    empty_squares_number = 0
                    result += "N"
                elif self.piece_placement[row][col] == Pieces.WHITE_BISHOP:
                    if empty_squares_number > 0: result += str(empty_squares_number)
                    empty_squares_number = 0
                    result += "B"
                elif self.piece_placement[row][col] == Pieces.WHITE_PAWN:
                    if empty_squares_number > 0: result += str(empty_squares_number)
                    empty_squares_number = 0
                    result += "P"
                elif self.piece_placement[row][col] == Pieces.BLACK_KING:
                    if empty_squares_number > 0: result += str(empty_squares_number)
                    empty_squares_number = 0
                    result += "k"
                elif self.piece_placement[row][col] == Pieces.BLACK_QUEEN:
                    if empty_squares_number > 0: result += str(empty_squares_number)
                    empty_squares_number = 0
                    result += "q"
                elif self.piece_placement[row][col] == Pieces.BLACK_ROOK:
                    if empty_squares_number > 0: result += str(empty_squares_number)
                    empty_squares_number = 0
                    result += "r"
                elif self.piece_placement[row][col] == Pieces.BLACK_KNIGHT:
                    if empty_squares_number > 0: result += str(empty_squares_number)
                    empty_squares_number = 0
                    result += "n"
                elif self.piece_placement[row][col] == Pieces.BLACK_BISHOP:
                    if empty_squares_number > 0: result += str(empty_squares_number)
                    empty_squares_number = 0
                    result += "b"
                elif self.piece_placement[row][col] == Pieces.BLACK_PAWN:
                    if empty_squares_number > 0: result += str(empty_squares_number)
                    empty_squares_number = 0
                    result += "p"
            if empty_squares_number > 0: result += str(empty_squares_number)
            if row > 0: result += "/"
                
        # Описываем чья очередь хода
        if self.whose_turn == Sides.WHITE:
            result += " w "
        else:
            result += " b "
        
        # Описываем можно ли делать рокировку
        temp_string = ""
        if self.white_can_kings_side_castling:
            temp_string += "K"
        if self.white_can_queens_side_castling:
            temp_string += "Q"
        if self.black_can_kings_side_castling:
            temp_string += "k"
        if self.black_can_queens_side_castling:
            temp_string += "q"
        if temp_string == "":
            temp_string = "-"
        result += (temp_string + " ")
        
        # Описываем проходную клетку
        if self.en_passant_target_square is not None:
            result += chr(ord('a') + self.en_passant_target_square[1])
            result += chr(ord('1') + self.en_passant_target_square[0])
        else:
            result += "-"
        
        # Описываем счетчик полуходов
        result += (" " + str(self.halfmove_clock) + " ")
        
        # Описываем номер хода
        result += str(self.fullmove_number)
        
        # Возвращаем результат
        return result
        
    # Выводит позицию на печать (используется для теста)
    def show(self):
    
        # Печатаем "шапку"
        print("\n--- printing position ---\n")
        
        # Печатаем расстановку фигур
        print("piece_placement:")
        for row in range(7, -1, -1):
            temp_string = ""
            for col in range(8):
                if self.piece_placement[row][col] is None:
                    temp_string += "xx"
                elif self.piece_placement[row][col] == Pieces.WHITE_KING:
                    temp_string += "wk"
                elif self.piece_placement[row][col] == Pieces.WHITE_QUEEN:
                    temp_string += "wq"
                elif self.piece_placement[row][col] == Pieces.WHITE_ROOK:
                    temp_string += "wr"
                elif self.piece_placement[row][col] == Pieces.WHITE_KNIGHT:
                    temp_string += "wn"
                elif self.piece_placement[row][col] == Pieces.WHITE_BISHOP:
                    temp_string += "wb"
                elif self.piece_placement[row][col] == Pieces.WHITE_PAWN:
                    temp_string += "wp"
                elif self.piece_placement[row][col] == Pieces.BLACK_KING:
                    temp_string += "bk"
                elif self.piece_placement[row][col] == Pieces.BLACK_QUEEN:
                    temp_string += "bq"
                elif self.piece_placement[row][col] == Pieces.BLACK_ROOK:
                    temp_string += "br"
                elif self.piece_placement[row][col] == Pieces.BLACK_KNIGHT:
                    temp_string += "bn"
                elif self.piece_placement[row][col] == Pieces.BLACK_BISHOP:
                    temp_string += "bb"
                elif self.piece_placement[row][col] == Pieces.BLACK_PAWN:
                    temp_string += "bp"
                if col < 7: temp_string += " "
            print(temp_string)
            
        # Печатаем кто ходит
        if self.whose_turn == Sides.WHITE:
            print("\nwhose_turn: WHITE")
        else:
            print("\nwhose_turn: BLACK")
            
        # Печатаем информацию о возможности рокироваться
        print("white_can_kings_side_castling: %s" % self.white_can_kings_side_castling)
        print("white_can_queens_side_castling: %s" % self.white_can_queens_side_castling)
        print("black_can_kings_side_castling: %s" % self.black_can_kings_side_castling)
        print("black_can_queens_side_castling: %s" % self.black_can_queens_side_castling)
        
        # Печатаем информацию о возможности бить пешку на проходе, а именно какое проходимое поле
        if self.en_passant_target_square is None:
            print("en_passant_target_square: None")
        else:
            temp_string = "en_passant_target_square: "
            temp_string += chr(ord('a') + self.en_passant_target_square[1])
            temp_string += chr(ord('1') + self.en_passant_target_square[0])
            print(temp_string)
            
        # Печатаем счетчик полуходов
        print("halfmove_clock: %s" % self.halfmove_clock)
        
        # Печатаем номер хода
        print("fullmove_number: %s" % self.fullmove_number)
        
        # Печатаем запись FEN диаграммы
        print("\nnotation: %s\n" % self.notation())
        

# Создает начальную позицию
def initial():
    initial_position_notation = 'rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1'
    return Position(notation=initial_position_notation)

